#ifndef CPPWEBPAGE_CONFIG_HPP
#define CPPWEBPAGE_CONFIG_HPP

#ifndef CPPWEBPAGE_STATIC

    // Windows platforms
    #ifdef CPPWEBPAGE_EXPORTS

        // From DLL side, we must export
        #define CPPWEBPAGE_API __declspec(dllexport)

    #else

        // From client application side, we must import
        #define CPPWEBPAGE_API __declspec(dllimport)

    #endif

    // For Visual C++ compilers, we also need to turn off this annoying C4251 warning.
    // You can read lots ot different things about it, but the point is the code will
    // just work fine, and so the simplest way to get rid of this warning is to disable it
    #ifdef _MSC_VER

        #pragma warning(disable : 4251)

    #endif

#else

    // No specific directive needed for static build
    #define CPPWEBPAGE_API

#endif

// SAL annotations in the Source-code Annotation Language. Used by Microsoft tools for documentation.
#define __in 
#define __out
#define __in_opt
#define __nullterminated 

#endif // CPPWEBPAGE_CONFIG_HPP