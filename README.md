cppwebpage-m
============

Embed an HTML control in your own window using C++ with static or dll library. CPPWebPage-m is fork of CPPWebPage, a C++ implementation of CWebPage from codeproject.

Original project: http://www.codeproject.com/Articles/3365/Embed-an-HTML-control-in-your-own-window-using-pla

### Bulding cppwebpage-m

To build cppwebpage-m static library on your computer you will need these packages:

1. meson
2. ninja
3. mingw-w64 (i686) or Microsoft Visual C++ compiler

Notice: If you have many compilers then use environment variable CXX for specify
    set CXX=cl.exe

Git clone the sources:
    git clone https://bitbucket.org/RedSkotina/cppwebpage-m

Run meson and build cppwebpage-m:
    cd cppwebpage-m
    mkdir build
    cd build
    meson .. 
    ninja